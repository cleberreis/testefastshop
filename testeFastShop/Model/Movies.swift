//
//  Movies.swift
//  testeFastShop
//
//  Created by Cleber Reis on 17/02/19.
//  Copyright © 2019 Cleber Reis. All rights reserved.
//

import Foundation

struct Movies: Codable {
    let page: Int
    let totalResults: Int
    let totalPages: Int
    let results: [Results]
    
    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
}

struct Results: Codable {
    let voteCount: Int
    let id: Int
    let video: Bool
    let voteAverage: Double
    let title: String
    let popularity: Double
    let posterPath: String
    let originalLanguage: String
    let originalTitle: String
    let overview: String
    let releaseDate: String
    let backdropPath: String
    
    enum CodingKeys: String, CodingKey {
        case voteCount = "vote_count"
        case voteAverage = "vote_average"
        case posterPath = "poster_path"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case releaseDate = "release_date"
        case backdropPath = "backdrop_path"
        case id, video, title, popularity, overview
    }
}
