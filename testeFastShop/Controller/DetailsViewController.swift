//
//  DetailsViewController.swift
//  testeFastShop
//
//  Created by Cleber Reis on 17/02/19.
//  Copyright © 2019 Cleber Reis. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var imgCapa: UIImageView!
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblOverview: UITextView!
    
    var movies: Results!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "\(movies.title)"
        
        self.imgCapa.download(from: "https://image.tmdb.org/t/p/w200" + "\(movies.backdropPath)")
        self.imgPoster.download(from: "https://image.tmdb.org/t/p/w200" + "\(movies.posterPath)")
        self.lblTitulo.text = movies.originalTitle
        self.lblOverview.text = movies.overview
        
    }
    
}
