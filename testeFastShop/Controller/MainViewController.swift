//
//  ViewController.swift
//  testeFastShop
//
//  Created by Cleber Reis on 17/02/19.
//  Copyright © 2019 Cleber Reis. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var movies: [Results] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Buscando Filmes"
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let urlString = "https://api.themoviedb.org/3/movie/popular?api_key=e189d4496f73d5ba5527453de08159c6&language=en-US&page=1"
        guard let url = URL(string: urlString) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil{
                print(error!.localizedDescription)
            }
            guard let data = data else {return}
            do{
                let result = try JSONDecoder().decode(Movies.self, from: data)
                
                self.movies = result.results
                
                DispatchQueue.main.async {
                    //print(result)
                    self.title = "Teste Fast Shop"
                    self.tableView.reloadData()
                }
                
            }catch let jsonError{
                print(jsonError)
            }
            }.resume()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! DetailsViewController
        let movieSelected = movies[tableView.indexPathForSelectedRow!.row]
        vc.movies = movieSelected
    }
    
    
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        //        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        
        let movie = self.movies[indexPath.row]
        
        cell.textLabel?.text = movie.originalTitle
        cell.detailTextLabel?.text = "\(movie.voteAverage)"
        cell.imageView?.download(from: "https://image.tmdb.org/t/p/w200" + "\(movie.posterPath)")
        
        return cell
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //
    //        let selectedMovie = self.movies[indexPath.row]
    //
    //        let detailMovie = MovieDetailViewController()
    //        detailMovie.title = selectedMovie.originalTitle
    //        detailMovie.lblOverview.text = selectedMovie.overview
    //
    //        self.present(UINavigationController(rootViewController: detailMovie), animated: true, completion: nil)
    //
    //    }
    
}
